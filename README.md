# ae_literal module 0.3.34

ae_literal is a Python multi-platform module project based on the [__Kivy__ Framework](https://kivy.org) 
and some portions of the [__ae__ namespace(Application Environment)](https://ae.readthedocs.io "ae on rtd").

the source code is available at [Gitlab](https://gitlab.com/ae-group/ae_literal) maintained by the user group ae-group.

additional credits to:

* [__Erokia__](https://freesound.org/people/Erokia/) and 
  [__plasterbrain__](https://freesound.org/people/plasterbrain/) at
  [freesound.org](https://freesound.org) for the sounds.
* [__iconmonstr__](https://iconmonstr.com/interface/) and
  [__Google__](https://fonts.google.com/icons?icon.set=Material+Symbols) for the icon images.

